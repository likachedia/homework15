package com.example.homework15

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.asLiveData
import com.example.homework15.DataStoreManager.Companion.ADDRESS
import com.example.homework15.DataStoreManager.Companion.NAME
import com.example.homework15.DataStoreManager.Companion.PHONE_NUMBER
import kotlinx.coroutines.flow.map

val USER_DATASTORE = "userInfo"
val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = USER_DATASTORE)
class DataStoreManager(val context: Context) {


    companion object {

        val NAME = stringPreferencesKey("NAME")
        val PHONE_NUMBER = stringPreferencesKey("PHONE_NUMBER")
        val ADDRESS = stringPreferencesKey("ADDRESS")

    }



    suspend fun savetoDataStore(phonebook: PhoneBook) {
        context.dataStore.edit {

        it[NAME] = phonebook.name
        it[PHONE_NUMBER] = phonebook.phoneNumber
        it[ADDRESS] = phonebook.address

        }

    }

    suspend fun getFromDataStore() = context.dataStore.data.map {
        PhoneBook(it[NAME]?:"",
                    it[PHONE_NUMBER]?:"",
                    it[ADDRESS]?:""
            )
    }.asLiveData()

}