package com.example.homework15.base

import android.content.Context
import androidx.lifecycle.ViewModel


abstract class BaseViewModel(context: Context): ViewModel()