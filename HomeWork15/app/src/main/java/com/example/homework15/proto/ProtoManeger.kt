package com.example.homework15.proto

import android.content.Context
import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.dataStore
import androidx.lifecycle.LiveData
import androidx.lifecycle.asLiveData
import com.example.homework15.SettingsProto
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import okio.IOException

val Context.settingsDataStore: DataStore<SettingsProto> by dataStore(
    fileName = "settings.pb",
    serializer = SettingsProtoSerializer
)
class ProtoManager(val context: Context) {

    init {
        context.settingsDataStore
    }

    val settingsFlow: LiveData<SettingsProto> = context.settingsDataStore.data
        .catch { e ->
            if (e is IOException) {
                Log.e("TAG", "Error reading sort order preferences: $e")
                emit(SettingsProto.getDefaultInstance())
            } else {
                throw e
            }
        }.asLiveData()


    suspend fun getData() : SettingsProto {
        return try {
            context.settingsDataStore.data.first()
        } catch (e : Exception) {
            e.printStackTrace()
            SettingsProto.getDefaultInstance()
        }
    }


    suspend fun updateSettings(lastName:String, email:String, url:String, gender:String) {
        context.settingsDataStore.updateData {  pref ->
            pref.toBuilder()
                .setEmail(email)
                .setImgUrl(url)
                .setGender(gender)
                .setLastName(lastName)
                .build()
        }
    }
}