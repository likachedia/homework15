package com.example.homework15.model

import android.content.Context
import com.example.homework15.DataStoreManager
import com.example.homework15.base.BaseViewModel
import com.example.homework15.proto.ProtoManager


class UserProfileViewModel(context: Context): BaseViewModel(context) {

    val dataStoreManager = DataStoreManager(context)
    val protoManeger = ProtoManager(context)
}