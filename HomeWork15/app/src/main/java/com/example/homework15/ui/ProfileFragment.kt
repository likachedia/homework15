package com.example.homework15.ui

import android.util.Log
import androidx.appcompat.view.menu.MenuView
import androidx.datastore.dataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.homework15.DataStoreManager
import com.example.homework15.PhoneBook
import com.example.homework15.R
import com.example.homework15.base.BaseFragment
import com.example.homework15.databinding.FragmentProfileBinding
import com.example.homework15.model.UserProfileViewModel
import com.example.homework15.setImage
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.invoke
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException


class ProfileFragment : BaseFragment<FragmentProfileBinding, UserProfileViewModel>(FragmentProfileBinding::inflate) {
    override fun getViewModelClass() = UserProfileViewModel::class.java
    override var useSharedViewModel = true
  //  val firstName = stringPreferencesKey("firstName")

  //  private val USER_FIRST_NAME = stringPreferencesKey("user_first_name")
  //  private val USER_LAST_NAME = stringPreferencesKey("user_last_name")
   // lateinit var dataStoreManager:DataStoreManager
    override fun start() {
       // dataStoreManager = DataStoreManager(requireContext())
      binding.profileImg.setImage(requireContext(), "https://reqres.in/i mg/faces/1-image.jpg")
      viewLifecycleOwner.lifecycleScope.launch {
            getData()
            listeners()
        }

    }

    suspend fun writeData() {
        viewModel.dataStoreManager.savetoDataStore(
            PhoneBook(binding.firstName.text.toString(), binding.phoneNumber.text.toString(),
                    binding.address.text.toString()
                )
        )
        viewModel.protoManeger.updateSettings(binding.lastName.text.toString(), binding.emailName.text.toString(),
            "\"https://reqres.in/i mg/faces/1-image.jpg\"", binding.gender.text.toString()
        )
    }

    private suspend fun listeners() {
        binding.editProfile.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                withContext(IO) {
                    writeData()
                }

            }
            findNavController().navigate(R.id.action_profileFragment_to_editPorfileFragment)
        }
    }
    private suspend fun getData() {

            viewModel.dataStoreManager.getFromDataStore().observe(viewLifecycleOwner, {
                Log.i("step", "1")
                binding.firstName.text = it.name
                binding.phoneNumber.text = it.address
                binding.address.text = it.phoneNumber

            })

            binding.lastName.text = viewModel.protoManeger.getData().lastName.toString()
            binding.gender.text = viewModel.protoManeger.getData().gender.toString()
            binding.emailName.text = viewModel.protoManeger.getData().email.toString()
            binding.profileImg.setImage(requireContext(), viewModel.protoManeger.getData().imgUrl)
    }
}