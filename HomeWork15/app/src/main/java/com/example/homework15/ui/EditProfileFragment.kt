package com.example.homework15.ui

import android.util.Log
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.homework15.DataStoreManager
import com.example.homework15.PhoneBook
import com.example.homework15.R
import com.example.homework15.base.BaseFragment
import com.example.homework15.databinding.FragmentEditProfileBinding
import com.example.homework15.model.UserProfileViewModel
import com.example.homework15.setImage
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class EditProfileFragment : BaseFragment<FragmentEditProfileBinding, UserProfileViewModel>(FragmentEditProfileBinding::inflate) {
    override fun getViewModelClass() = UserProfileViewModel::class.java
    override var useSharedViewModel = true
    //lateinit var dataStoreManager: DataStoreManager
    private val USER_FIRST_NAME = stringPreferencesKey("user_first_name")
    private val USER_LAST_NAME = stringPreferencesKey("user_last_name")

    override fun start() {
        //dataStoreManager = DataStoreManager(requireContext())

        viewLifecycleOwner.lifecycleScope.launch {
            withContext(Main) {
                viewModel.dataStoreManager.getFromDataStore().observe(viewLifecycleOwner, {

                    binding.etPhoneNumber.hint = it.phoneNumber
                    binding.etAddress.hint = it.address
                    binding.etFirstName.hint = it.name

                })

                Log.i("proto", viewModel.protoManeger.getData().lastName)
                binding.etProfileImg.hint = viewModel.protoManeger.getData().imgUrl
                binding.etLastName.hint = viewModel.protoManeger.getData().lastName
                binding.etGender.hint =  viewModel.protoManeger.getData().gender
                binding.etEmail.hint = viewModel.protoManeger.getData().email
                binding.etProfileImg.hint = viewModel.protoManeger.getData().imgUrl.toString()
            }



        }


        listeners()
    }

    private fun listeners() {
        binding.Save.setOnClickListener {
            viewLifecycleOwner.lifecycleScope.launch {
                withContext(IO) {

                    Log.i("step", "3")
                    writeData()
                }

            }
            findNavController().navigate(R.id.action_editPorfileFragment_to_profileFragment)
        }
    }
    suspend fun writeData() {
        Log.i("step", "2")
        viewModel.dataStoreManager.savetoDataStore(

            PhoneBook(binding.etFirstName.text.toString(), binding.etPhoneNumber.text.toString(),
                binding.etAddress.text.toString()
            )
        )

        viewModel.protoManeger.updateSettings(binding.etLastName.text.toString(), binding.etEmail.text.toString(),
                binding.etProfileImg.text.toString(), binding.etGender.text.toString()
            )

    }
}