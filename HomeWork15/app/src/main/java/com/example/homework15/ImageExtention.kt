package com.example.homework15

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.setImage(context: Context, url:String) {
    if(url == null) {
        setImageResource(R.drawable.ic_launcher_background)
    } else {
        Glide.with(context).load(url).placeholder(R.drawable.ic_launcher_background)
            .error(R.drawable.ic_launcher_background).into(this)
    }

}